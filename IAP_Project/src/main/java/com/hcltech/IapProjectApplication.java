package com.hcltech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IapProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IapProjectApplication.class, args);
	}

}
