package com.hcltech.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcltech.entity.User;

@Repository 
public interface UserRepository extends JpaRepository<User,Integer> {

}
