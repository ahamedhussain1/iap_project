package com.hcltech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcltech.entity.Admin;
import com.hcltech.repository.AdminRepository;

@Service
public class AdminService {
	
	@Autowired
	AdminRepository adminRepo;
	
	//create
	public Admin CreateAdmin(Admin admin) {
		return adminRepo.save(admin);
	}
	
	//display
	public List<Admin> DisplayAdmin(){
		return adminRepo.findAll();
	}
	
	//delete
	public void DeleteAdmin(int AdminId) {
		adminRepo.deleteById(AdminId);
	}
	
	//update
	public Admin UpdateAdmin(Admin admin,int AdminId) {
		Admin a = adminRepo.findById(AdminId).get();
		a.setAdminName(admin.getAdminName());
		a.setEmail(admin.getEmail());
		a.setPassword(admin.getPassword());
		return adminRepo.save(a);
	}
	
	// To Login as Admin
	public int loginCheckAdmin(String email, String password) {
		List <Admin> admins = adminRepo.findAll();
		int adminId = 0;
		for(Admin admin:admins) {
			if(admin.getEmail().compareTo(email) == 0 && admin.getPassword().compareTo(password) == 0) {
				adminId = admin.getAdminId();
			}
		}
		return adminId;
	}

}
