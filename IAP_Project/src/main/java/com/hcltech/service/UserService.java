package com.hcltech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcltech.entity.User;
import com.hcltech.exception.ResourceNotFoundException;
import com.hcltech.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepo;
	
	
	//create
	public User createUser(User user) {
		return userRepo.save(user);
	}
	
	//display
	public List<User> displayUser(){
		return userRepo.findAll();
	}
	
	//delete
	public void deleteuser(int userId) {
		User existingUser = userRepo.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("user","Userid",userId));
		userRepo.delete(existingUser);
	}
	
	//update
	public User updateUser(int userId,User user) {
		User existingUser = userRepo.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("user","Userid",userId));
		
		existingUser.setUserName(user.getUserName());
		existingUser.setEmail(user.getEmail());
		existingUser.setPassword(user.getPassword());
		existingUser.setDOJ(user.getDOJ());
		existingUser.setTotalExp(user.getTotalExp());
		existingUser.setHclLaptop(user.getHclLaptop());
		existingUser.setCurrentLocation(user.getCurrentLocation());
		existingUser.setPersonalLaptop(user.getPersonalLaptop());
		existingUser.setWorkLocation(user.getWorkLocation());
		existingUser.setNonItExp(user.getNonItExp());
		return userRepo.save(existingUser);
	}
	
	//LOGIN CHECK
	public int loginCheck (String email, String passWord) {
		List <User> users = userRepo.findAll();
		int userId = 0;
		for(User user:users) {
			if(user.getEmail().compareTo(email) == 0 && user.getPassword().compareTo(passWord) == 0) {
				userId = user.getUserId();
			}
		}
		return userId;
	}

}
